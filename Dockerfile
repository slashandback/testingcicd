# Gunakan image resmi Kotlin sebagai base image
FROM kotlin:latest-jdk

# Menyalin berkas Gradle
COPY . /app

# Pindah ke direktori proyek
WORKDIR /app

# Menjalankan tugas Gradle untuk membangun proyek
RUN ./gradlew build

# Menyalin berkas JAR hasil kompilasi ke dalam gambar
CMD ["cp", "build/libs/*.jar", "/app/app.jar"]
